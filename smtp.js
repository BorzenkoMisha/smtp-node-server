const SMTPServer = require('smtp-server').SMTPServer;
const MailParser = require('mailparser').MailParser;
const config = require('./config/index');
const logger = require('./lib/logger');
const fetch = require('node-fetch');

const mailparser = new MailParser();

config.smtp.options.onData = (stream, session, callback) => {
  stream.pipe(mailparser);
  stream.on('end', callback);
};

const server = new SMTPServer(config.smtp.options);

// On parse end
mailparser.on('end', (mail) => {
  mail.from = mail.from[0];
  fetch(`http://${config.http.host}:${config.http.port}/mails`, {
    method: 'POST',
    body: JSON.stringify(mail)
  })
  .then((response) => {
    if (response.status !== 200) {
      return logger.info(`Looks like there was a problem. Status Code: ${response.status}`);
    }
    return response.json().then((data) => (logger.info(data)));
  })
  .catch((err) => (logger.error(err)));
});

// Error handler
server.on('error', (err) => (logger.info('Error %s', err.message)));

server.listen(config.smtp.port, config.smtp.host,
    () => (logger.info(`SMTP server start listening ${config.smtp.host}:${config.smtp.port}`)));
