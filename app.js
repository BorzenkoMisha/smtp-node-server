const Glue = require('glue');
const logger = require('./lib/logger');
const Manifest = require('./config/manifest');
require('babel-register')({
  plugins: ['transform-async-to-generator']
});
Glue.compose(Manifest, { relativeTo: __dirname }, (err, server) => {
  server.start((e) => {
    if (e) {
      logger.error(e);
    }
  });
});
