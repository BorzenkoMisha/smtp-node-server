const Code = require('code');
const expect = Code.expect;
const Lab = require('lab');
const Glue = require('glue');
const manifest = require('../config/manifest');
const mongoose = require('mongoose');
require('babel-register')({
  plugins: ['transform-async-to-generator']
});

const lab = exports.lab = Lab.script();

Glue.compose(manifest, { relativeTo: process.cwd() }, (err, server) => {
  server.start(() => { });
  const Project = mongoose.model('Project');
  lab.describe('Projects', () => {
    lab.beforeEach((done) => {
      Project.remove({}, (error) => {
        if (error) {
          return error;
        }
        return true;
      });
      done();
    });
    lab.it('Route shoud return 200 and list of projects', done => {
      const options = {
        method: 'GET',
        url: '/projects'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.result).to.be.instanceof(Array);
        done();
      });
    });
    lab.it('Route shoud return 400 and error if id length less than 24 characters', done => {
      const options = {
        method: 'GET',
        url: '/projects/1'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(400);
        expect(response.result).to.be.instanceof(Object);
        expect(response.result.error).to.equal('Bad Request');
        done();
      });
    });
    lab.it('Route shoud return 400 and error if id length more than 24 characters', done => {
      const options = {
        method: 'GET',
        url: '/projects/11111111111111111111111111'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(400);
        expect(response.result).to.be.instanceof(Object);
        expect(response.result.error).to.equal('Bad Request');
        done();
      });
    });
    lab.it('Route shoud return 404 and error if project with id is not exist', done => {
      const options = {
        method: 'GET',
        url: '/projects/1234abcd1234abcd1234abcd'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(404);
        expect(response.result).to.be.instanceof(Object);
        expect(response.result.error).to.equal('Not Found');
        done();
      });
    });
    lab.it('Route shoud return 200 and proejct', done => {
      const createOptions = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(createOptions, (res) => {
        const detailOptions = {
          method: 'GET',
          url: '/projects/' + res.result._id
        };
        server.inject(detailOptions, (response) => {
          expect(response.statusCode).to.equal(200);
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Route shoud return 200 and created project without users', done => {
      const options = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'labTestProjects'
        }
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(200);
        expect(response.result).to.be.instanceof(Object);
        expect(response.result.title).to.be.equal('labTestProjects');
        expect(response.result.users).to.be.instanceof(Array);
        expect(response.result._id.toString()).to.have.length(24);
        done();
      });
    });
    lab.it('if you try to create project without payload data, route shoud return 400 and error message', done => {
      const options = {
        method: 'POST',
        url: '/projects'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(400);
        expect(response.result.error).to.equal('Bad Request');
        expect(response.result).to.be.instanceof(Object);
        done();
      });
    });
    lab.it('if you try to create project with title characters length less than 3, route shoud return 400 and error message', done => {
      const options = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 't'
        }
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(400);
        expect(response.result.error).to.equal('Bad Request');
        expect(response.result).to.be.instanceof(Object);
        done();
      });
    });
    lab.it('If you try to create project with name which already exist in database, route shoud return 500 response', done => {
      const options = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(options, () => {
        server.inject(options, (response) => {
          expect(response.statusCode).to.equal(500);
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Delete project, route shoud return 200 response', done => {
      const optionsCreate = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(optionsCreate, (res) => {
        const optionsDelete = {
          method: 'DELETE',
          url: '/projects/' + res.result._id
        };
        server.inject(optionsDelete, (response) => {
          expect(response.statusCode).to.equal(200);
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Route shoud return 404 and error if project with id is not exist', done => {
      const options = {
        method: 'DELETE',
        url: '/projects/123456789123456789123456'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(404);
        expect(response.result.error).to.equal('Not Found');
        expect(response.result).to.be.instanceof(Object);
        done();
      });
    });
    lab.it('Route shoud return 404 and error if project id length not 24 characters', done => {
      const options = {
        method: 'DELETE',
        url: '/projects/23456789123456789123456'
      };
      server.inject(options, (response) => {
        expect(response.statusCode).to.equal(400);
        expect(response.result.error).to.equal('Bad Request');
        expect(response.result).to.be.instanceof(Object);
        done();
      });
    });
    lab.it('Update project, route shoud return 200 response', done => {
      const optionsCreate = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(optionsCreate, (res) => {
        const optionsDelete = {
          method: 'PUT',
          url: '/projects/' + res.result._id,
          payload: {
            title: 'newTestName'
          }
        };
        server.inject(optionsDelete, (response) => {
          expect(response.statusCode).to.equal(200);
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Update project with title length less than 3, route shoud return 400 response', done => {
      const optionsCreate = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(optionsCreate, (res) => {
        const optionsDelete = {
          method: 'PUT',
          url: '/projects/' + res.result._id,
          payload: {
            title: '1'
          }
        };
        server.inject(optionsDelete, (response) => {
          expect(response.statusCode).to.equal(400);
          expect(response.result.error).to.equal('Bad Request');
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Update project without payload, route shoud return 400 response', done => {
      const optionsCreate = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(optionsCreate, (res) => {
        const optionsDelete = {
          method: 'PUT',
          url: '/projects/' + res.result._id
        };
        server.inject(optionsDelete, (response) => {
          expect(response.statusCode).to.equal(400);
          expect(response.result.error).to.equal('Bad Request');
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
    lab.it('Update project and set new id, route shoud return 400 response', done => {
      const optionsCreate = {
        method: 'POST',
        url: '/projects',
        payload: {
          title: 'test'
        }
      };
      server.inject(optionsCreate, (res) => {
        const optionsDelete = {
          method: 'PUT',
          url: '/projects/' + res.result._id,
          payload: {
            title: 'test',
            _id: '12'
          }
        };
        server.inject(optionsDelete, (response) => {
          expect(response.statusCode).to.equal(400);
          expect(response.result.error).to.equal('Bad Request');
          expect(response.result).to.be.instanceof(Object);
          done();
        });
      });
    });
  });
});
