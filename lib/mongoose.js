const mongoose = require('mongoose');
const glob = require('glob');
const logger = require('./logger');

exports.register = (plugin, options, next) => {
  mongoose.connect(options.uri, (err) => {
    if (err) {
      return logger.error(err);
    }
    return logger.info('Server connected');
  });
  glob.sync('../models/**.js', { cwd: __dirname }).forEach(require);
  next();
};

exports.register.attributes = {
  name: 'mongoose'
};
