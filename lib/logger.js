const winston = require('winston');
const config = require('../config/index');
require('winston-mongodb');

const logger = new (winston.Logger)(
  {
    exitOnError: false,
    transports: [
      new (winston.transports.Console)({
        level: 'info' // info level and above (warn, error)
      }),
      new (winston.transports.MongoDB)({
        levels: 'warn', // warn level and above (error)
        db: config.mongoose.uri,
        collection: 'logs'
      })
    ]
  }
);

module.exports = logger;
