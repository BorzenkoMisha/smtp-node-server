const Joi = require('joi');
const logger = require('../lib/logger');
exports.register = (plugin, options, next) => {
  const MailsController = require('../controllers/mails');

  const mails = new MailsController();
  plugin.route([
    {
      method: 'GET',
      path: '/mails',
      config: {
        tags: ['api'],
        description: 'Get all mails',
        notes: 'Returns list of mails',
        handler: mails.list
      }
    },
    {
      method: 'GET',
      path: '/mails/{id}',
      config: {
        tags: ['api'],
        notes: 'Returns a mail item by the id passed in the path',
        description: 'Get mail',
        handler: mails.detail,
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          }
        }
      }
    },
    {
      method: 'POST',
      path: '/mails',
      config: {
        tags: ['api'],
        description: 'Create mail',
        notes: 'Adds new mail in database and returns it',
        validate: {
          payload: Joi.object({
            headers: Joi.object().keys({
              'x-project': Joi.string().trim().required(),
              'x-user-email': Joi.string().trim().email().required()
            }).options({ allowUnknown: true }),
            html: Joi.string(),
            subject: Joi.string().max(78).required(),
            from: Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            }),
            to: Joi.array().items(Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            })),
            cc: Joi.array().items(Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            })),
          }).options({ allowUnknown: true }),
          failAction: (request, reply, source, error) => {
            logger.error(error);
          }
        },
        handler: mails.create
      }
    },
    {
      method: 'PUT',
      path: '/mails/{id}',
      config: {
        tags: ['api'],
        description: 'Update mail',
        notes: 'Updates mail by the id and returns it',
        handler: mails.update,
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          },
          payload: Joi.object({
            headers: Joi.object().keys({
              'x-project': Joi.string().trim().required(),
              'x-user-email': Joi.string().trim().email().required()
            }).options({ allowUnknown: true }),
            html: Joi.string(),
            subject: Joi.string().max(78).required(),
            from: Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            }),
            to: Joi.array().items(Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            })),
            cc: Joi.array().items(Joi.object({
              address: Joi.string().trim().email().required(),
              name: Joi.string().trim().allow('')
            })),
          }).options({ allowUnknown: true }),
          failAction: (request, reply, source, error) => {
            logger.error(error);
          }
        }
      }
    },
    {
      method: 'DELETE',
      path: '/mails/{id}',
      config: {
        tags: ['api'],
        description: 'Delete mail',
        notes: 'Delete mail by the id and returns it',
        handler: mails.remove,
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          }
        }
      }
    }
  ]);
  next();
};

exports.register.attributes = {
  name: 'mails_routes',
};
