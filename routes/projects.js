const Joi = require('joi');
exports.register = (plugin, options, next) => {
  const ProjectsController = require('../controllers/projects');
  const projects = new ProjectsController();
  plugin.route([
    {
      method: 'GET',
      path: '/projects',
      config: {
        tags: ['api'],
        description: 'Get all projects',
        notes: 'Returns list of projects',
        handler: projects.list
      }
    },
    {
      method: 'GET',
      path: '/projects/{id}',
      config: {
        handler: projects.detail,
        tags: ['api'],
        notes: 'Returns a project item by the id passed in the path',
        description: 'Get project',
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          }
        }
      }
    },
    {
      method: 'POST',
      path: '/projects',
      config: {
        handler: projects.create,
        tags: ['api'],
        description: 'Create project',
        notes: 'Adds new project in database and returns it',
        validate: {
          payload: {
            title: Joi.string().min(2).max(15)
          }
        }
      }
    },
    {
      method: 'PUT',
      path: '/projects/{id}',
      config: {
        handler: projects.update,
        tags: ['api'],
        description: 'Update project',
        notes: 'Updates project by the id and returns it',
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          },
          payload: {
            title: Joi.string().trim().min(2).max(15)
          }
        }
      }
    },
    {
      method: 'DELETE',
      path: '/projects/{id}',
      config: {
        tags: ['api'],
        description: 'Delete project',
        notes: 'Delete project by the id and returns it',
        handler: projects.remove,
        validate: {
          params: {
            id: Joi.string().trim().length(24)
          }
        }
      }
    }
  ]);

  next();
};

exports.register.attributes = {
  name: 'projects_routes',
};
