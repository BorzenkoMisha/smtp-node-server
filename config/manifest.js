const config = require('./');
const manifest = {
  server: {
    connections: {
      router: {
        stripTrailingSlash: true,
        isCaseSensitive: false
      },
      routes: {
        cors: true,
        validate: {
          options: {
            abortEarly: false
          }
        }
      }
    },
  },
  connections: [{
    port: config.http.port
  }],
  registrations: [
    {
      plugin: {
        register: 'inert'
      }
    },
    {
      plugin: {
        register: 'vision'
      }
    },
    {
      plugin: {
        register: 'hapi-swagger',
        options: {
          info: {
            title: 'NIX mailer documentation',
          }
        }
      }
    },
    {
      plugin: {
        register: './lib/mongoose',
        options: config.mongoose
      }
    },
    {
      plugin: './routes/mails'
    },
    {
      plugin: './routes/projects'
    },
  ]
};
module.exports = manifest;
