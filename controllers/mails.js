const mongoose = require('mongoose');
const Boom = require('boom');
const logger = require('../lib/logger');
const config = require('../config');
const Mail = mongoose.model('Mail');


module.exports = class MailsController {

  async list(request, reply) {
    try {
      const result = await Mail.find({}).populate('_project').populate('_user').sort({ created: 'descending' });
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrapp(e));
    }
  }

  async detail(request, reply) {
    try {
      const result = await Mail.findOne({ _id: request.params.id }).populate('_project').populate('_user');
      if (!result) {
        throw Boom.create(404, `Mail with ${request.params.id} not found`);
      }
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async create(request, reply) {
    try {
      const title = request.payload.headers['x-project'] || config.mongoose.project.default;
      const userEmail = request.payload.headers['x-user-email'];
      const result = await Mail.save(request.payload, title, userEmail);
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async update(request, reply) {
    try {
      const result = await Mail.findOne({ _id: request.params.id });
      if (!result) {
        throw Boom.create(404, `Mail with ${request.params.id} not found`);
      }
      const res = await Mail.update({ _id: request.params.id }, { $set: request.payload }, { new: true });
      return reply(res);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async remove(request, reply) {
    try {
      const result = await Mail.findOne({ _id: request.params.id });
      if (!result) {
        throw Boom.create(404, `Mail with ${request.params.id} not found`);
      }
      const res = await Mail.remove({ _id: request.params.id });
      return reply(res);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

};
