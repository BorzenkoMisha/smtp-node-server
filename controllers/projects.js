const mongoose = require('mongoose');
const Boom = require('boom');
const logger = require('../lib/logger');
const Project = mongoose.model('Project');

module.exports = class ProjectsController {

  async list(request, reply) {
    try {
      const result = Project.find({}).sort({ created: 'descending' });
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async detail(request, reply) {
    try {
      const result = await Project.findOne({ _id: request.params.id });
      if (!result) {
        throw Boom.create(404, `Project with ${request.params.id} not found`);
      }
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async create(request, reply) {
    try {
      const result = Project.create(request.payload);
      return reply(result);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async update(request, reply) {
    try {
      const result = await Project.findOne({ _id: request.params.id });
      if (!result) {
        throw Boom.create(404, `Project with ${request.params.id} not found`);
      }
      const res = await Project.update({ _id: request.params.id }, request.payload, { new: true });
      return reply(res);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

  async remove(request, reply) {
    try {
      const result = await Project.findOne({ _id: request.params.id });
      if (!result) {
        throw Boom.create(404, `Project with ${request.params.id} not found`);
      }
      const res = await Project.remove({ _id: request.params.id });
      return reply(res);
    } catch (e) {
      logger.error(e);
      return reply(Boom.wrap(e));
    }
  }

};
