const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: String,
  address: { type: String, required: true }
});

const schema = new Schema({
  users: [userSchema],
  isEditting: { type: Boolean, default: false },
  title: { type: String, unique: true, required: true, background: false }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
});

mongoose.model('Project', schema);
