const Boom = require('boom');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: String,
  address: { type: String, required: true }
});

const mailSchema = new Schema({
  _project: { type: Schema.Types.ObjectId, ref: 'Project' },
  _user: { type: Schema.Types.ObjectId, ref: 'User' },
  html: String,
  text: String,
  subject: { type: String, required: true },
  headers: { type: Object, required: true },
  from: userSchema,
  to: [userSchema],
  cc: [userSchema],
  attachments: Array,
  isRead: { type: Boolean, default: false },
  isFavorite: { type: Boolean, default: false },
  isDeleted: { type: Boolean, default: false }
}, {
  timestamps: {
    createdAt: 'created',
    updatedAt: 'updated'
  }
});

mailSchema.statics.save = async (data, projectTitle, userEmail) => {
  const user = await mongoose.model('User').findOne({ email: userEmail });
  if (!user) {
    throw Boom.create(404, `User with email ${userEmail} not found`);
  }
  const project = await mongoose.model('Project').findOne({ title: projectTitle });
  if (!project) {
    throw Boom.create(404, `Project with title ${projectTitle} not found`);
  }
  const mail = data;
  mail._project = project;
  mail._user = user;
  const result = await mongoose.model('Mail').create(mail);
  console.log(result);
  return result;
};

mongoose.model('Mail', mailSchema);
