const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  email: { type: String, required: true, uniqe: true },
  projects: [{ type: Schema.Types.ObjectId, ref: 'Project' }]
});

mongoose.model('User', schema);
